#include <iostream>
#include <string>

#include "StateMachine.h"

int main()
{
	std::cout << "ACCUM VOCABULARY\n" << std::endl;

	StateMachine::instance().start();

	std::string text;
	while (StateMachine::instance().isRunning())
	{
		std::getline(std::cin, text);
		StateMachine::instance().handleMessage(std::make_shared<Message>(text));
	}



	return 0;
}
