#pragma once

#include <string>

class Message
{
public:
	enum class MENU_POINT: size_t
	{
		UNDEFINED    = 0,
		MENU_POINT_1 = 1,
		MENU_POINT_2 = 2,
		MENU_POINT_3 = 3,
		MENU_POINT_4 = 4,
		MENU_POINT_5 = 5
	};

	Message(const std::string& message);
	~Message();

	const std::string& getMessage() const;
	MENU_POINT getMenuPoint() const;

private:
	std::string m_message;
	MENU_POINT m_menuPoint;

	static const size_t m_menuPointsCount;
};
