#include "StateMachine.h"
#include "View.h"
#include <typeinfo>

#include <iostream>

namespace
{

}

/************* MenuState ***************/

void InitialState::handleEnter()
{
	// 1. Проверяем наличие базы. Если нет - создаём
	// 2. Проверить наличие нужных таблиц
	// 3. Обновить показатели (даты и счётчики слов)

	// Переходим в меню
	StateMachine::instance().switchState<MenuState>();
	return;

	// При возникновении какой-либо ошибки - вырубаем машину состояний
	StateMachine::instance().stop();
}

void InitialState::handleMessage(const std::shared_ptr<Message> message)
{
}

void InitialState::handleExit()
{

}

/************* MenuState ***************/

void MenuState::handleEnter()
{
	View::instance().showMainMenu();
}

void MenuState::handleMessage(const std::shared_ptr<Message> message)
{
	switch (message->getMenuPoint())
	{
	case Message::MENU_POINT::MENU_POINT_1: // Словарь
		StateMachine::instance().switchState<VocabularyState>();
		break;
	case Message::MENU_POINT::MENU_POINT_2: // Тренировка
		StateMachine::instance().switchState<TrainingState>();
		break;
	case Message::MENU_POINT::MENU_POINT_3: // Выход
		StateMachine::instance().stop();
		break;
	default:
		StateMachine::instance().switchState<MenuState>();
	}
}

void MenuState::handleExit()
{

}

/************* VocabularyState ***************/

void VocabularyState::handleEnter()
{
	View::instance().showVocabularyMainMenu();
}

void VocabularyState::handleMessage(const std::shared_ptr<Message> message)
{
	switch (message->getMenuPoint())
	{
	case Message::MENU_POINT::MENU_POINT_1: // Загрузить блок слов
		// С помощью команды модели выводим слова

		View::instance().showVocabularyMainMenu(false);
		break;

	case Message::MENU_POINT::MENU_POINT_2: // Добавить слово
		StateMachine::instance().switchState<VocabularyAddWordState>();
		break;

	case Message::MENU_POINT::MENU_POINT_3: // Удалить слово
		StateMachine::instance().switchState<VocabularyDeleteWordState>();
		break;

	case Message::MENU_POINT::MENU_POINT_4: // Назад
		StateMachine::instance().switchState<MenuState>();
		break;

	default:
		View::instance().showVocabularyOfferAnswer();
	}
}

void VocabularyState::handleExit()
{
}

/************* VocabularyAddWordState ***************/

void VocabularyAddWordState::handleEnter()
{
	if (m_isWordInputed)
	{
		View::instance().showVocabularyOfferTranslateWord();
	}
	else
	{
		View::instance().showVocabularyOfferInputWord();
	}
}

void VocabularyAddWordState::handleMessage(const std::shared_ptr<Message> message)
{
	if (message->getMessage().empty()) // Пустая строка - выход
	{
		StateMachine::instance().switchState<VocabularyState>();
	}
	else
	{
		if (m_isWordInputed)
		{
			m_translate = message->getMessage();

			// Тут обращаемся к модели и добавляем слово

			StateMachine::instance().switchState<VocabularyState>();
		}
		else
		{
			m_word = message->getMessage();
			m_isWordInputed = true;

			StateMachine::instance().switchState<VocabularyAddWordState>();
		}
	}
}

void VocabularyAddWordState::handleExit()
{
}

/************* VocabularyDeleteWordState ***************/

void VocabularyDeleteWordState::handleEnter()
{
	View::instance().showVocabularyOfferDeleteWord();
}

void VocabularyDeleteWordState::handleMessage(const std::shared_ptr<Message> message)
{
	if (! message->getMessage().empty()) // Пустая строка - выход
	{
		// Удаляем слово
	}

	StateMachine::instance().switchState<VocabularyState>();
}

void VocabularyDeleteWordState::handleExit()
{
}

/************* TrainingState ***************/

void TrainingState::handleEnter()
{
	m_isWordInputed = false;

	// model->startTraining
	showNextWord();
}

void TrainingState::handleMessage(const std::shared_ptr<Message> message)
{
	if (m_isWordInputed)
	{
		showNextWord();

		m_isWordInputed = false;
	}
	else
	{
		if (! m_isNext || message->getMessage().empty())
		{
			StateMachine::instance().switchState<MenuState>();
		}
		else
		{
			if (true/*model->checkTranslation(message->getMessage())*/)
			{
				// Верно!
			}
			else
			{
				// Неверно! Верный перевод: фывыфв.
			}

			m_isWordInputed = true;
		}
	}
}

void TrainingState::handleExit()
{
	// model->stopTraining();
}

void TrainingState::showNextWord()
{
	View::instance().showTrainingHeader();
	m_isNext = true; // model->showNextTrainingWord();
	if (m_isNext)
	{
		View::instance().showTrainingOfferTranslateWord();
	}
	else
	{
		// Напиши, что слов больше нет
	}
}

/************ StateMachine ***************/

StateMachine &StateMachine::instance()
{
	static StateMachine sm;
	return sm;
}

void StateMachine::start()
{
	m_isRunning = true;
	switchState<InitialState>();
}

void StateMachine::stop()
{
	m_isRunning = false;
}

void StateMachine::handleMessage(const std::shared_ptr<Message> message)
{
	if (! m_isRunning)
	{
		return;
	}

	if (m_currentState)
	{
		m_currentState->handleMessage(message);
	}
}

bool StateMachine::isRunning() const
{
	return m_isRunning;
}

StateMachine::StateMachine()
	: m_isRunning(false)
{
}

StateMachine::~StateMachine()
{
}

template<class T>
void StateMachine::switchState()
{
	if (m_currentState)
	{
		m_currentState->handleExit();
	}

	if (!m_currentState || typeid(T) != typeid(*m_currentState.get()))
	{
		m_currentState.reset();
		m_currentState = std::make_shared<T>();
	}

	m_currentState->handleEnter();
}
