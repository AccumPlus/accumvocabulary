#pragma once

#include <memory>

#include "IState.h"
#include "Message.h"

/*********** Начальное состояние ********/
class InitialState: public IState
{
public:
	virtual void handleEnter() override;
	virtual void handleMessage(const std::shared_ptr<Message> message) override;
	virtual void handleExit() override;
};
/*********************************************/

/*********** Состояния главного меню ********/
/**
 * @brief Состояние главного меню
 */
class MenuState: public IState
{
public:
	virtual void handleEnter() override;
	virtual void handleMessage(const std::shared_ptr<Message> message) override;
	virtual void handleExit() override;
};
/*********************************************/

/*********** Состояния работы со словарём **/

/**
 * @brief Начальное состояние работы со словарём
 */
class VocabularyState: public IState
{
public:
	virtual void handleEnter() override;
	virtual void handleMessage(const std::shared_ptr<Message> message) override;
	virtual void handleExit() override;

private:
};

/**
 * @brief Состояние добавления слова
 */

class VocabularyAddWordState: public IState
{
public:
	virtual void handleEnter() override;
	virtual void handleMessage(const std::shared_ptr<Message> message) override;
	virtual void handleExit() override;

private:
	std::string m_word;
	std::string m_translate;
	bool m_isWordInputed = false;
};

class VocabularyDeleteWordState: public IState
{
public:
	virtual void handleEnter() override;
	virtual void handleMessage(const std::shared_ptr<Message> message) override;
	virtual void handleExit() override;
};

/*********************************************/

class TrainingState: public IState
{
public:
	virtual void handleEnter() override;
	virtual void handleMessage(const std::shared_ptr<Message> message) override;
	virtual void handleExit() override;
private:
	void showNextWord();

	bool m_isWordInputed;
	bool m_isNext;
};

class StateMachine
{
public:
	static StateMachine& instance();
	/**
	 * @brief запустить машину состояний
	 */
	void start();
	/**
	 * @brief остановить машину состояний
	 */
	void stop();
	/**
	 * @brief Обработать сообщение
	 */
	void handleMessage(const std::shared_ptr<Message> message);

	template<class T>
	void switchState();

	bool isRunning() const;

private:
	StateMachine();
	~StateMachine();

	StateMachine(const StateMachine&) = delete;
	StateMachine& operator=(const StateMachine&) = delete;

	std::shared_ptr<IState> m_currentState;

	bool m_isRunning;
};
