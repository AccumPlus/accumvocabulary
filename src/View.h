#pragma once

class View
{
public:
	static View& instance();

	/**
	 * @brief Показать главное меню
	 */
	void showMainMenu() const;
	/**
	 * @brief Показать основное меню редактирования словаря
	 */
	void showVocabularyMainMenu(const bool isMainMenu = true) const;
	/**
	 * @brief Показать предложение ввести ответ
	 */
	void showVocabularyOfferAnswer() const;
	/**
	 * @brief Показать предложение ввести слово
	 */
	void showVocabularyOfferInputWord() const;
	/**
	 * @brief Показать предложение ввести перевод слова
	 */
	void showVocabularyOfferTranslateWord() const;
	/**
	 * @brief Показать предложение ввести номер удаляемого слова
	 */
	void showVocabularyOfferDeleteWord() const;
	/**
	 * @brief Показать заголовок тренировки слов
	 */
	void showTrainingHeader() const;
	/**
	 * @brief Показать предложение ввести перевод слова
	 */
	void showTrainingOfferTranslateWord() const;

private:
	View();
	~View();

	View(const View&) = delete;
	View& operator=(const View&) = delete;
};
