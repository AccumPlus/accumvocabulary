#pragma once

#include <memory>

class Message;

class IState
{
public:
	virtual void handleEnter() = 0;
	virtual void handleMessage(const std::shared_ptr<Message> message) = 0;
	virtual void handleExit() = 0;
};
