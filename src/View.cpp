#include "View.h"

#include <iostream>

View &View::instance()
{
	static View view;
	return view;
}

void View::showMainMenu() const
{
	system("clear");
	std::cout << "ГЛАВНОЕ МЕНЮ\n\n 1. Редактирование словаря\n 2. Тренировка\n 3. Выход\n\nВыбор: ";
}

void View::showVocabularyMainMenu(const bool isMainMenu) const
{
	if (isMainMenu)
	{
		system("clear");
		std::cout << "МЕНЮ РАБОТЫ СО СЛОВАРЁМ\n\n 1. Показать блок слов\n";
	}
	else
	{
		std::cout << "МЕНЮ РАБОТЫ СО СЛОВАРЁМ\n\n 1. Показать ещё блок слов\n";
	}
	std::cout << " 2. Добавить слово\n 3. Удалить слово\n 4. Назад\n\n";
	showVocabularyOfferAnswer();
}

void View::showVocabularyOfferAnswer() const
{
	std::cout << "Выбор: ";
}

void View::showVocabularyOfferInputWord() const
{
	system("clear");
	std::cout << "МЕНЮ РАБОТЫ СО СЛОВАРЁМ\n\nВведите слово (пустая строка для отмены): ";
}

void View::showVocabularyOfferTranslateWord() const
{
	std::cout << "Введите перевод (пустая строка для отмены): ";
}

void View::showVocabularyOfferDeleteWord() const
{
	system("clear");
	std::cout << "МЕНЮ РАБОТЫ СО СЛОВАРЁМ\n\nВведите номер удаляемого слова (пустая строка для отмены): ";
}

void View::showTrainingHeader() const
{
	system("clear");
	std::cout << "ТРЕНИРОВКА СЛОВ\n\n";
}

void View::showTrainingOfferTranslateWord() const
{
	std::cout << "Введите перевод (пустая строка для возврата): ";
}

View::View()
{
}

View::~View()
{
}
