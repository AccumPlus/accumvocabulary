#include <stdexcept>

#include "Message.h"

const size_t Message::m_menuPointsCount = 5;

Message::Message(const std::string &message)
	: m_menuPoint(MENU_POINT::UNDEFINED)
{
	m_message = message;

	if (m_message.size() == 1)
	{
		try
		{
			size_t number = std::stoul(message, 0, 10);
			if (number <= m_menuPointsCount && number != 0)
			{
				m_menuPoint = MENU_POINT(number);
			}
		}
		catch (std::invalid_argument)
		{
			m_menuPoint = MENU_POINT::UNDEFINED;
		}
	}
}

Message::~Message()
{
}

const std::string &Message::getMessage() const
{
	return m_message;
}

Message::MENU_POINT Message::getMenuPoint() const
{
	return m_menuPoint;
}


