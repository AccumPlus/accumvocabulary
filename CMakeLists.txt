cmake_minimum_required(VERSION 3.2)

project(accumVocabulary)

set (CMAKE_BUILD_TYPE Debug)

subdirs(src SQLiteCpp)
